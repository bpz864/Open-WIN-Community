---
layout: default
title: Benefits of Open Research
parent: Open WIN Community
has_children: false
nav_order: 3
---

# Benefits of Open Research
{: .fs-9 }

Why open research is good for you (and everyone else)
{: .fs-6 .fw-300 }

---

The benefits of data sharing to the scientific community are widely agreed upon. But does data sharing also benefit individual scientists? In the talk below [Dr Laurence Hunt](https://www.win.ox.ac.uk/people/laurence-hunt) argues that data sharing may carry tangible benefits to one’s own research that can outweigh any potential associated costs. He then discusses some practicalities on getting started with data sharing, through the lens of his own perspective of sharing in previous projects.

<div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/668640629?h=84c5b7a7e1&amp;badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen style="position:absolute;top:0;left:0;width:100%;height:100%;" title="Data sharing and reproducibility.&amp;nbsp;Dr Laurence Hunt (January 2022)"></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>
