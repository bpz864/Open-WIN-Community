---
layout: default
title: 2.1 Your GitLab account
parent: Tutorials
grand_parent: Git and GitLab
has_children: false
nav_order: 4
---


# Access and connect to your GitLab account
{: .fs-8 }

Access your GitLab account
{: .fs-6 .fw-300 }

---
## Who can access GitLab?
To use the WIN GitLab you will need to register for a WIN IT computing account. Please register for a WIN computing account following this [guide on the WIN intranet](https://sharepoint.nexus.ox.ac.uk/sites/NDCN/FMRIB/IT/User%20Guides/Computing%20Accounts%20-%20requesting%2c%20using%20and%20closing.aspx).

Once you have a computing account, you can access GitLab by visiting [https://git.fmrib.ox.ac.uk](https://git.fmrib.ox.ac.uk) in a web browser and log in (using the LDAP tab) with your WIN IT username and password.

### SSH authentication
The git server identifies and authenticates you (or rather your computer) using an [SSH key pair](https://www.ssh.com/academy/ssh/public-key-authentication). This means we need to create a key pair to authenticate the connection from your local computer to the remote repository.

#### Creating the SSH key pair
From your terminal, create a public/private key by entering the command below:
`ssh-keygen -t rsa`

You will be asked to provide a file name for the key. Leave this blank to accept the default location.

This will create two files in your `[home].ssh` directory:

`id_rsa`
your private key (keep this safe)
`id_rsa.pub`
your public key which you will give to GitLab.

You will be asked to create a password. This password will be used by the git system to read the contents of your ssh key file (i.e. confirm your identity) when you send information to your repository. Do not use the same password as your WIN IT account password.

#### Give the id_rsa.pub key to GitLab
1. Once you have created the key, enter `cd ~` in your terminal, then `cat .ssh/id_rsa.pub` to print the contents of the SSH key file into the terminal. It will be a long sting of letters and numbers which begins with `ssh-rsa` and ends with your computer name.
2.Copy all of the ssh key printed above.
3. Sign in to [git.fmrib.ox.ac.uk](https://git.fmrib.ox.ac.uk) using the LDAP tab and your WIN IT username and password.
4. On the top bar, in the top right corner, select your avatar.
5. Select Preferences.
6. On the left sidebar, select SSH Keys (the key symbol).
7. In the Key box, paste the contents of your public key. If you manually copied the key, make sure you paste the entire key, including the `ssh-rsa` and computer name.
8. In the Title box, type a description, like Work Laptop or Home Workstation.
9. Optional. In the Expires at box, select an expiration date.
10. Select Add key.

### Windows
To create SSH key pairs on **Windows**, follow the instructions on the [WIN IT help page](https://sharepoint.nexus.ox.ac.uk/sites/NDCN/FMRIB/IT/User%20Guides/SSH.aspx#title16) until you are asked to transfer the key to a server (this will not be neccessary here). Then, move on to follow the instructuctions on the [Git User Guide](https://sharepoint.nexus.ox.ac.uk/sites/NDCN/FMRIB/IT/User%20Guides/Git.aspx).

### More information
Take a look at the [WIN IT help pages](https://sharepoint.nexus.ox.ac.uk/sites/NDCN/FMRIB/IT/User%20Guides/SSH.aspx#title11) (internal only) and the [GitLab Docs wiki](https://docs.gitlab.com/ee/ssh/) for more information on usage of SSH key pairs. 
