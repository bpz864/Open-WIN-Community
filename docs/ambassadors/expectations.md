---
layout: default
title: Expectations of Ambassadors
parent: Open WIN Ambassadors
grand_parent: Open WIN Community
has_children: false
nav_order: 5
---

# Expectations of Ambassadors
{: .fs-9 }

The behaviours and activities we would like Ambassadors to commit to
{: .fs-6 .fw-300 }

---

We would like WIN Open Ambassadors to undertake the following activities:

### Training and education
- Commitment to learn about open research practices, understand our own knowledge gaps and know where to signpost effectively.
- Support the learning of other researchers through answering questions on slack, mailing lists or in private communications.

### Advocacy
- Advocate for open research practices and the use of Open WIN Tools in their research groups, Department and WIN.
- Speak to researchers about their own experience with open research practices and infrastructure during research group and Departmental presentations and training events.

### Contributing to the Open WIN Infrastructure
- Contribute to and publish documentation on Open WIN tools (see [this issue to see what documentation may entail](https://git.fmrib.ox.ac.uk/open-science/community/Open-WIN-Community/-/issues/25)).
- Where possible, use the Open WIN infrastructure to share one of your research outputs.
- Contribute to WIN Open Science policy.

## Time commitment
- Participate in Ambassadors meetings as per the [published schedule](../programme#schedule).
- Engage in training, advocacy, and contributing to the Open WIN infrastructure for 2-3 hours per month, in addition to the above call schedule.
- If you attend all calls and do an additional 2.5 hours a month as recommended, you will commit approximately 7 days over the course of the programme.

In return for their time and commitment, various [rewards and incentives](../benefits) will be offered to the Open WIN Ambassadors. We will continually reflect on and refine our expectations and commitments as we learn from our experience.

We will work to support Ambassadors in all these activities, with communication channels, training, presentation materials and defined routes of development. This support will develop as the project grows, and you will have an open invitation to suggest any additional ways we could support you in growing our community.
