---
layout: default
title: Open Data
has_children: true
nav_order: 4
has_toc: false
---

# Data Sharing Decision Framework
{: .fs-9 }

Click on the images below to learn more about the practicalities of sharing your data.
{: .fs-6 .fw-300 }

---

[![data-why](../../img/img-data-why.png)](../data/why) &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; [![data-can](../../img/img-data-can.png)](../data/can-i)  
<br><br>
[![data-how](../../img/img-data-how.png)](../data/how) &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; [![data-when](../../img/img-data-when.png)](../data/when)



<!-- ### General Data Protection Regulation (GDPR) compliant data sharing policy
The database has the capability to share data at between specified individuals, openly to all WIN members, or externally based on the requirements of the research lead.

#### De-identification
Before data is shared externally, it will be checked against a list of defined criteria to ensure the data are appropriately de-identified and any risk of identification of individual participants is negligible. The criteria for de-identification will include removal of identifying facial features (defacing), the removal of personal data from raw [dicom](https://en.wikipedia.org/wiki/DICOM) images, and the removal of any linkage with consent or experimental participant identification numbers.

#### Data usage Agreement
Individuals accessing shared data will additionally be required to agree to a Data Usage Policy, where they explicitly confirm that they will not attempt to re-identify participants, nor share the data with any third party who has not signed the same agreement.

The Data Usage Agreement and de-identification process is being developed with the support of Departmental and University level [Information Security and Compliance](https://www.infosec.ox.ac.uk) teams.

#### Quality control
WIN members will also be encouraged to run and share the results of predefined quality control algorithms, so anyone accessing the data can have a ready measure of image quality.

[![For WIN members](../../img/btn-win.png)](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/tools/data/#for-win-members)      [![For external researchers](../../img/btn-external.png)](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/tools/data/#for-external-researchers) -->

<!-- Coming soon
{: .label .label-yellow }

**THIS TOOL IS CURRENTLY IN DEVELOPMENT. PLEASE REFER TO THE INFORMATION BELOW TO UNDERSTAND THE AIM AND AMBITION OF THIS PROJECT. THE "HOW TO" GUIDE WILL BE BUILT BY THE COMMUNITY AND TOOL DEVELOPERS IN THE COMING MONTHS.**

<br> -->



<!-- ### Benefits
#### Version control ![version-control](../../img/icon-version-control.png)
Coming soon
{: .label .label-yellow }

#### Citable research output ![doi](../../img/icon-doi.png)
Coming soon
{: .label .label-yellow }

#### Reproducible methods detail ![reproduce](../../img/icon-reproduce.png)
Coming soon
{: .label .label-yellow }
 -->





<!-- ## Working group members (alphabetically)
We are grateful to the following WIN members for their contributions to developing the Open Data server.
- [Stuart Clare](https://www.win.ox.ac.uk/people/stuart-clare)
- [Cassandra Gould van Praag]
- [Dave Flitney](https://www.win.ox.ac.uk/people/david-flitney)
- [Clare Mackay](https://www.win.ox.ac.uk/people/clare-mackay)
- [Duncan Mortimer](https://www.win.ox.ac.uk/people/duncan-mortimer)
- Paul Semple
- Duncan Smith
- [Matt South](https://www.win.ox.ac.uk/people/matthew-south) -->
